# i3-lm

i3 layout manager

## config

* edit `load.py`
  - change workspace
  - change location of `i3-save-tree` json
  - edit shell commands as necessary
  - depending on your setup, may want to add apps

* create/edit `i3-save-tree` json as necessary
  - checkout for tips on working with `i3-save-tree` json
    * https://i3wm.org/docs/layout-saving.html
  - you can checkout the json i put together
    * https://gitlab.com/ktsukik/dotfiles/blob/master/desktop/.config/i3/dashboard.json

## restore layout

`$ ./load.py`

## license
[GNU GPLv3](/LICENSE)
