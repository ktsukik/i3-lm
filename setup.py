#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
## setup.py
from distutils.core import setup

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='i3-lm',
    version='0.1',
    description='i3 layout manager',
    long_description=readme
    autho='tsu',
    author_email='tsu@keemail.me',
    url='https://gitlab.com/ktsukik/i3-lm',
    license=license,
    packages=find_packages(exclude=('data'))
)
