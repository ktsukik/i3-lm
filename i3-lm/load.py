#!/usr/bin/env python
# load.py
import subprocess
import os

# config
workspace = 4
layout=os.path.expanduser('~/.config/i3/dashboard.json')

cmds = [
    {'name': 'ranger', 'cmd': 'ranger', 'opts': ''},
    {'name': 'neofetch', 'cmd': 'neofetch | lolcat', 'opts': ''},
    {'name': 'tty-clock', 'cmd': 'tty-clock', 'opts': '-c'},
    {'name': 'alsamixer', 'cmd': 'alsamixer', 'opts': ''},
    {'name': 'mpsyt', 'cmd': 'mpsyt', 'opts': ''}
]

urxvtstrings = []

for cmd in cmds:
    name = cmd['name']
    binary = cmd['cmd']
    opts = cmd['opts']
    string = 'urxvt -name {} -e $SHELL -c "{} {}; $SHELL" && $SHELL &'.format(name, binary, opts)
    urxvtstrings.append(string)


i3cmd = 'i3-msg "workspace {}; append_layout {}"'.format(workspace, layout)

subprocess.run(i3cmd, shell=True)

for cmd in urxvtstrings:
    subprocess.run(cmd, shell=True)

